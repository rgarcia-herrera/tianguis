from django.urls import reverse
from django.contrib.gis.db import models
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _
from djmoney.models.fields import MoneyField
from django.utils.text import slugify
from time import time_ns


# class Zone(models.Model):


class Category(models.Model):
    title = models.CharField(max_length=255,
                             verbose_name=_("title"),
                             help_text=_("title"))

    category_slug = models.SlugField(unique=True)
    description = models.TextField(_('description'))

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = _("Categories")



class Subcategory(models.Model):
    title = models.CharField(max_length=255,
                             verbose_name=_("title"),
                             help_text=_("title"))

    description = models.TextField(_('description'))

    subcategory_slug = models.SlugField(unique=True)

    category = models.ForeignKey(Category,
                                 on_delete=models.CASCADE)
    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = _("Subcategories")




class Advertisement(models.Model):

    author = models.ForeignKey(User,
                               verbose_name=_('author'),
                               on_delete=models.CASCADE)

    title = models.CharField(max_length=100,
                             verbose_name=_("title"),
                             help_text=_("title"))

    description = models.TextField(_('description'))

    date_from = models.DateTimeField(auto_now_add=True)
    date_thru = models.DateTimeField()

    price = MoneyField(_("price"),
                       max_digits=12,
                       decimal_places=2,
                       default_currency='MXN',
                       blank=True, null=True)

    category = models.ForeignKey(Category,
                                 verbose_name=_('category'),
                                 on_delete=models.CASCADE)

    subcategory = models.ForeignKey(Subcategory,
                                    verbose_name=_('category'),
                                    on_delete=models.CASCADE)

    ad_status = models.CharField(_("Ad status"),
                              max_length=15,
                              default='new',
                              choices=(('new', 'new'),
                                       ('published', 'published'),
                                       ('canceled', 'canceled'),
                                       ('expired', 'expired')))


    item_status = models.CharField(_("condiciones"),
                                   max_length=15,
                                   blank=True, null=True,
                                   choices=(('nuevo', 'nuevo'),
                                            ('como_nuevo', 'como nuevo'),
                                            ('poco_uso', 'poco uso'),
                                            ('usado', 'usado'),
                                            ('viejo', 'viejo')))


    point = models.PointField(blank=True, null=True)

    def front_pic(self):
        if self.picture_set.filter(front=True).count() > 0:
            return self.picture_set.filter(front=True).first()
        elif self.picture_set.count() > 0:
            return self.picture_set.first()
        else:
            return None


    def get_absolute_url(self):
        return reverse("ad_detail", kwargs={"ad_id": self.pk})


    def __str__(self):
        return f"{self.title} ({self.ad_status})"



def pic_path(instance, filename):
    username = instance.advertisement.author.username
    ad_id = instance.advertisement.id
    title = slugify(instance.advertisement.title)[0:50]
    ns = str(time_ns())[-10:]
    return f"{username}/{ad_id}-{title}/{ns}/{filename}"

class Picture(models.Model):
    advertisement = models.ForeignKey(Advertisement,
                                      verbose_name=_('advertisement'),
                                      on_delete=models.CASCADE)

    image = models.ImageField(upload_to=pic_path)

    front = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.advertisement.title}"



class Search(models.Model):
    author = models.ForeignKey(User,
                               verbose_name=_('author'),
                               on_delete=models.CASCADE)

    search_terms = models.CharField(max_length=255,
                                    verbose_name=_("search terms"),
                                    help_text=_("search terms"))

    date_from = models.DateTimeField(auto_now_add=True)
    date_thru = models.DateTimeField()

    price_min = models.DecimalField(_("minimum price"),
                                     max_digits=12,
                                     decimal_places=2,
                                     blank=True, null=True)

    price_max = models.DecimalField(_("maximum price"),
                                    max_digits=12,
                                    decimal_places=2,
                                    blank=True, null=True)

    category = models.ForeignKey(Category,
                                 verbose_name=_('category'),
                                 on_delete=models.CASCADE)

    point = models.PointField(blank=True, null=True)



class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    city = models.CharField(max_length=100, blank=True, null=True)
    country_code = models.CharField(max_length=100, blank=True, null=True)
    country_name = models.CharField(max_length=100, blank=True, null=True)
    postal_code = models.CharField(max_length=100, blank=True, null=True)

    point = models.PointField(blank=True, null=True)

    km = models.PositiveSmallIntegerField(default=50)
