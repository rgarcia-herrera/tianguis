from django.shortcuts import redirect, render
from plaza.models import Advertisement, Category, Subcategory
from siteajax.toolbox import Ajax, AjaxResponse
from django.db.models import Q
from numpy import average, min, max
from django.core.paginator import Paginator



def get_center(ads):
    lon = []
    lat = []

    for ad in ads:
        lon.append(ad.point.coords[0])
        lat.append(ad.point.coords[1])

    return (average(lon),
            average(lat))


def get_bounds(ads):
    lon = []
    lat = []

    for ad in ads:
        lon.append(ad.point.coords[0])
        lat.append(ad.point.coords[1])

    return ((min(lon),
             min(lat)),
            (max(lon),
             max(lat)))


template = {
    'gallery': 'gallery.html',
    'list': 'ad_list.html',
    'map': 'map_search.html',
}


def search(request):
    category_slug = request.GET.get('cat', '')
    subcategory_slug = request.GET.get('subcat', 'all')
    mode = request.GET.get('mode', 'gallery')

    try:
        cat = Category.objects.get(category_slug=category_slug)
    except Category.DoesNotExist:
        return redirect('cat_index')

    if subcategory_slug is not None and subcategory_slug != 'all':

        try:
            subcat = Subcategory.objects.get(subcategory_slug=subcategory_slug)
        except Subcategory.DoesNotExist:
            return redirect('search',
                            category_slug=category_slug,
                            subcategory_slug='all')
        ads = Advertisement.objects.filter(category=cat,
                                           subcategory=subcat)
    else:
        subcategory_slug = 'all'
        ads = Advertisement.objects.filter(category=cat)


    if request.GET.get('item_status', False):
        if request.GET['item_status'] != 'cualquiera':
            ads = ads.filter(item_status=request.GET['item_status'])

    if request.GET.get('precio_desde', False):
        ads = ads.filter(price__gte=request.GET['precio_desde'])

    if request.GET.get('precio_hasta', False):
        ads = ads.filter(price__lte=request.GET['precio_hasta'])


    if request.GET.get('busca', False):
        busca = request.GET['busca']
        ads = ads.filter(Q(title__icontains=busca)
                         |Q(description__icontains=busca))

    paginator = Paginator(ads, 25)
    page_number = request.GET.get("page")
    ads_page = paginator.get_page(page_number)
        
    ajax: Ajax = request.ajax

    bounds = get_bounds(ads_page)

    if ajax:
        response = render(request,
                          template[mode],
                          {'ads': ads_page,
                           'bounds': bounds})
        response = AjaxResponse(response)

        return response

    return render(request, 'advertisement_filter.html',
                  {'ads': ads_page,
                   'bounds': bounds,
                   'categories': Category.objects.all(),
                   'category': cat,
                   'subcategory_slug': subcategory_slug,
                   'mode': mode})



def subcategories(request):
    """ returns subcategories of a category """
    category_slug = request.GET['cat']
    cat = Category.objects.get(category_slug=category_slug)
    subcats = cat.subcategory_set.all()
    ajax: Ajax = request.ajax

    if ajax:
        return AjaxResponse(render(request,
                                   'subcategories.html',
                                   {'subcats': subcats,}))

    return redirect('cat_index')



def cat_index(request):
    """this view is served at /"""

    cats = Category.objects.all()

    return render(request,
                  'index.html', {'cats': cats})



def ad_detail(request, ad_id):
    ad = Advertisement.objects.get(pk=ad_id)
    response = render(request, 'ad_detail.html', {'ad': ad,})
    return response



def pic_frame(request, ad_id, current_pic_id, mode='gallery'):
    ad = Advertisement.objects.get(pk=ad_id)
    current_pic = ad.picture_set.all()[int(current_pic_id) % ad.picture_set.count()]
    if ad.picture_set.count() > 1:
        previous_pic_id = (int(current_pic_id) - 1) % ad.picture_set.count()
        next_pic_id = (int(current_pic_id) + 1) % ad.picture_set.count()
        position = [True if p == int(current_pic_id) else False
                    for p in
                    range(0, ad.picture_set.count())]
    else:
        # just one pic
        previous_pic_id = None
        next_pic_id = None
        position = [True, ]

    if mode == 'gallery':
        template = 'pic_frame.html'
    elif mode == 'detail':
        template = 'pic_detail.html'

    response = render(request,
                      template,
                      {'ad': ad,
                       'current_pic': current_pic,
                       'current_pic_id': current_pic_id,
                       'position': position,
                       'previous_pic_id': previous_pic_id,
                       'next_pic_id': next_pic_id})
    return response
