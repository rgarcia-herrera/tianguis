# Generated by Django 4.2.4 on 2023-10-11 00:55

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('plaza', '0012_delete_taggeditem'),
    ]

    operations = [
        migrations.RenameField(
            model_name='advertisement',
            old_name='status',
            new_name='ad_status',
        ),
    ]
