# Generated by Django 4.2.4 on 2023-10-11 01:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('plaza', '0014_advertisement_item_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='advertisement',
            name='item_status',
            field=models.CharField(blank=True, choices=[('nuevo', 'nuevo'), ('como_nuevo', 'como nuevo'), ('poco_uso', 'poco uso'), ('usado', 'usado'), ('viejo', 'viejo')], max_length=15, null=True, verbose_name='condiciones'),
        ),
    ]
