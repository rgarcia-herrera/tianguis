# Generated by Django 4.2.4 on 2023-10-11 00:51

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('plaza', '0011_category_category_slug_subcategory_subcategory_slug'),
    ]

    operations = [
        migrations.DeleteModel(
            name='TaggedItem',
        ),
    ]
