from django.contrib.gis import admin
from plaza import models
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User





class SubcategoryInline(admin.TabularInline):
    model = models.Subcategory
    extra = 1


@admin.register(models.Category)
class CategoryAdmin(admin.ModelAdmin):
    search_fields = ['title', ]
    inlines = [SubcategoryInline, ]

class PictureInline(admin.StackedInline):
    model = models.Picture
    extra = 1
    
    
@admin.register(models.Advertisement)
class AdvertisementAdmin(admin.GISModelAdmin):
    list_display = ['title', 'date_from', 'date_thru', 'author', 'category', 'subcategory', 'ad_status']
    search_fields = ['title', 'description', 'author__username']
    autocomplete_fields = ['category', ]
    list_filter = ['ad_status',]
    readonly_fields = ['date_from',]
    inlines = [PictureInline,]    

    gis_widget_kwargs = {
        'attrs': {
            'default_lat': 19.3565,
            'default_lon': -97.9088,
            'default_zoom': 6
        }
    }



class ProfileInline(admin.StackedInline):
    model = models.Profile
    can_delete = False
    verbose_name_plural = "perfil"
    gis_widget_kwargs = {
        'attrs': {
            'default_lat': 19.3565,
            'default_lon': -97.9088,
            'default_zoom': 6
        }
    }

    

class UserAdmin(BaseUserAdmin):
    inlines = [ProfileInline]


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
