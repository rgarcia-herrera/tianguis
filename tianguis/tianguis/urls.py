"""
URL configuration for tianguis project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from plaza import views, models
from django.conf.urls.static import static
from django.conf import settings


urlpatterns = [
    path('admin/', admin.site.urls),
    path('subcats/', views.subcategories, name="subcats"),
    path("a/<ad_id>/pic/<current_pic_id>/<mode>", views.pic_frame, name='pic_frame'),
    path("a/<ad_id>/", views.ad_detail, name='ad_detail'),
    path('search/',  views.search, name='search'),
    path("__reload__/", include("django_browser_reload.urls")),
    path('',  views.cat_index, name='cat_index'),
]


urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
